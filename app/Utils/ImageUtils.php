<?php 
	
	class ImageUtils {

		const TMP_PATH = 'assets/img/tmp/';


		public static function move($name, $image)
		{
			$image->move('upload/profile/', $name . '.' .$image->getClientOriginalExtension());
			$url = asset('upload/profile/'. $name . '.' .$image->getClientOriginalExtension());

			return $url;
		}

		public static function move_emergency($name, $image)
		{
			$image->move('upload/emergency/', $name . '.' .$image->getClientOriginalExtension());
			$url = asset('upload/emergency/'. $name . '.' .$image->getClientOriginalExtension());

			return $url;
		}

		public static function move_audio($name, $image)
		{
			$image->move('upload/chamado_audio/', $name . '.' .$image->getClientOriginalExtension());
			$url = asset('upload/chamado_audio/'. $name . '.' .$image->getClientOriginalExtension());

			return $url;
		}


		public static function move_sponsor($name, $image)
		{
			$image->move('upload/sponsor/', $name . '.' .$image->getClientOriginalExtension());
			$url = asset('upload/sponsor/'. $name . '.' .$image->getClientOriginalExtension());

			return $url;
		}

		public static function move_tmp($name, $image)
		{			
			$image_name = $name . '.' .$image->getClientOriginalExtension();
			$image->move(self::TMP_PATH, $image_name);	
			$url = asset(self::TMP_PATH. $image_name);

			return $url;
		}
		
		public static function move_object($name, $image)
		{
			$image->move('assets/img/objeto/', $name . '.' .$image->getClientOriginalExtension());
			$url = asset('assets/img/objeto/'. $name . '.' .$image->getClientOriginalExtension());

			return $url;
		}

	}



?>