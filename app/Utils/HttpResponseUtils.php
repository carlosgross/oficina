<?php 
	
	class HttpResponseUtils {


		public static function error($errors)
		{
			return array(
				'success' => false,
				'errors'  => $errors);
		}

		public static function success($message)
		{
			return array(
				'success' => true,
				'message' => $message);
		}

		public static function successWithId($id, $message)
		{
			return array(
				'success' => true,
				'id'      => $id,
				'message' => $message);
		}

	}
?>