<?php

/*
| 
| @author Carlos Eduardo G. R. Ribas
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*********************************************************************************/
/* HOME                                                                          */
/*********************************************************************************/

Route::auth();
Route::get('/login', 'AdminController@login');
Route::get('/password/reset', 'AdminController@resetPassword');

Route::group(['middleware' => 'auth'], function () {

    /*********************************************************************************/
    /* PAINEL                                                                        */
    /*********************************************************************************/

        /*********************************************************************************/
        /* DASHBOARD                                                                     */
        /*********************************************************************************/
        Route::get('/', 'AdminController@dashboard');
        Route::get('/home', 'AdminController@dashboard');
        Route::get('/dashboard', 'AdminController@dashboard');

        /*********************************************************************************/
        /* PROFILE                                                                       */
        /*********************************************************************************/
        Route::get('perfil', 'AdminController@profile');
        Route::post('update-profile', 'AdminController@update_profile');

        /*********************************************************************************/
        /* CHANGE PASSOWRD                                                               */
        /*********************************************************************************/
        Route::get('change-password', 'AdminController@view_change_password');
        Route::post('change-password', 'AdminController@change_password');

        /*********************************************************************************/
        /* CALENDAR                                                                      */
        /*********************************************************************************/
        Route::get('calendario', 'AdminController@calendar');

    /*********************************************************************************/
    /* RESOURCES                                                                     */
    /*********************************************************************************/

        /*********************************************************************************/
        /* USER                                                                          */
        /*********************************************************************************/
        Route::resource('user', 'UserController');

});