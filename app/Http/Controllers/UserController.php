<?php

/*
| 
| @author Carlos Eduardo G. R. Ribas
|
*/

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Validator;
use App\User;

require_once app_path().'/Utils/IDFactory.php';

class UserController extends Controller {

	/**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

    $users = User::where('role', '<>', 'ADMIN')->get();
    return view('user/index', ['users' => $users, 'usuario' => 'active', 'usuarios_cadastrados' => 'active']);    

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

    return view('user/create', ['usuario' => 'active', 'novo_usuario' => 'active']);

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {

    $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
      'email' => 'required|email|max:255|unique:users',
      'image' => 'image',
    ]);
    

    if ($validator->fails()) {
        return redirect('user/create')->withErrors($validator)->withInput();
    }

    $user = new User;

    $user->name = $request->input('name');
    $user->email = $request->input('email');

    if($request->hasFile('image')) {
      $thumbnailImage = $request->file('image');
      $filename = $thumbnailImage->getClientOriginalName();
      $destinationPath = 'public/upload/users/'.$user->id;
      $thumbnailImage->move($destinationPath, $filename);
      $user->image = url('public/upload/users/'.$user->id.'/'.$filename);
    }

    $user->save();

    return redirect('user')->with('status', 'Cliente cadastrado com sucesso!');

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $user = User::find($id);

    return view('user/edit', ['user' => $user, 'usuario' => 'active']);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {

    $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
      'email' => 'required|email|max:255',
      'image' => 'image',
    ]);

    if ($validator->fails()) {
        return redirect('user/'.$id.'/edit')->withErrors($validator)->withInput();
    }

    $duplicate_email = User::where('email','=',$request->input('email'))->where('id','<>',$id)->get();

    if(count($duplicate_email) > 0) {
      return redirect('user/'.$id.'/edit')->with('error', 'E-mail já utilizado por outro cliente');
    }

    $user = User::find($id);

    $user->name = $request->input('name');
    $user->email = $request->input('email');

    if($request->hasFile('image')) {

      // remove old image folder
      $this->delTree('public/upload/users/'.$user->id);

      $thumbnailImage = $request->file('image');
      $filename = $thumbnailImage->getClientOriginalName();
      $destinationPath = 'public/upload/users/'.$user->id;
      $thumbnailImage->move($destinationPath, $filename);
      $user->image = url('public/upload/users/'.$id.'/'.$filename);
    }

    $user->save();

    return redirect('user/'.$id.'/edit')->with('status', 'Os dados do cliente foram atualizados com sucesso!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

    $user = User::find($id);

    if(count($user) > 0) {
      $name = $user->name;

      // remove image folder
      $this->delTree('public/upload/users/'.$user->id);

      $user->delete();

      return redirect('user')->with('status', 'Cliente '.$name.' e todos os dados relacionados a ele foram removidos com sucesso!');
    } else {
      return redirect('user')->with('status', 'Usuário não encontrado');
    }

  }

  // remove folder
  private function delTree($dir) { 
    $files = array_diff(scandir($dir), array('.','..')); 
    foreach ($files as $file) { 
      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
    } 
    return rmdir($dir); 
  }

}
