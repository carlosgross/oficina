<!DOCTYPE html>
<html>
    <head>
        @include('head')
    </head>
    <body class="hold-transition lockscreen">
        <!-- Automatic element centering -->
        <div class="lockscreen-wrapper">
          <div class="lockscreen-logo">
            <a href="">{{ config('app.app_name') }}</a>
          </div>
          <!-- User name -->
          <div class="lockscreen-name">Informe seu e-mail para recuperar sua senha</div>

          <!-- START LOCK SCREEN ITEM -->
          <div class="lockscreen-item">

            <!-- lockscreen credentials (contains the form) -->
            <form class="lockscreen-credentials" method="POST" action="{{ url('password/email') }}">
              {!! csrf_field() !!}
              <div class="input-group">
                <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}" required>

                <div class="input-group-btn">
                  <button type="submit" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
              </div>
            </form>
            <!-- /.lockscreen credentials -->

          </div>
          <!-- /.lockscreen-item -->

          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
          @endif

          @if ($errors->has('email'))
            <div class="help-block text-center">
              {{ $errors->first('email') }}
            </div>
          @endif

          <div class="text-center">
            <a href="{{ url('/') }}"><i class="fa fa-arrow-left"></i> Voltar para o formulário de login</a>
          </div>
          
        </div>
        <!-- /.center -->

        @include('core-scripts')

    </body>
</html>
