<!DOCTYPE html>
<html>
  <head>
  	@include('head')
  </head>
  <body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      
      @include('header')

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      @include('sidebar')

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Meu Perfil
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('perfil') }}"><i class="fa fa-user"></i> Perfil</a></li>
            <li class="active">Trocar senha</li>
          </ol>
        </section>        

        <!-- Main content -->
        <section class="content">

          @if (session('status'))
              <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ session('status') }}
              </div>
          @endif

          @if (session('error'))
              <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ session('error') }}
              </div>
          @endif

          <!-- Default box -->
          <form action="{{ url('change-password') }}" method="post">
            {!! csrf_field() !!}
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Trocar senha</h3>
              </div>
              <div class="box-body">

                <div class="form-group has-feedback {{ $errors->has('current_password') ? ' has-error' : '' }}">
                    <label for="current_password">Senha atual</label>
                    <input type="password" class="form-control" placeholder="Senha" name="current_password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('current_password'))
                        <span class="help-block">
                            {{ $errors->first('current_password') }}
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Senha</label>
                    <input type="password" class="form-control" placeholder="Senha" name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password_confirmation">Confirmar senha</label>
                    <input type="password" class="form-control" placeholder="Confirmar senha" name="password_confirmation">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            {{ $errors->first('password_confirmation') }}
                        </span>
                    @endif
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{ url('perfil') }}" class="btn btn-default">voltar</a>
                <button type="submit" class="btn btn-primary">Salvar</button>
              </div>
              <!-- /.box-footer-->
            </div>
            <!-- /.box -->
          </form>

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      @include('footer')

      <!-- Control Sidebar -->
      @include('control-sidebar')
    </div>
    <!-- ./wrapper -->

    @include('core-scripts')

  </body>
</html>
