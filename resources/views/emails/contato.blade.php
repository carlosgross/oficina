<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			<b>Nome: </b> {{ $nome }}
			<br>
			<b>E-mail: </b> {{ $email }}
			<br>
			<b>Cidade: </b> {{ $cidade }}
			<br>
			<b>Estado: </b> {{ $estado }}
			<br>
			<b>Telefone: </b> {{ $telefone }}
			<br><br>
			<b>Mensagem: </b> {{ $mensagem }}
		</div>
	</body>
</html>
