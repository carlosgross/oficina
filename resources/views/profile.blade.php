<!DOCTYPE html>
<html>
  <head>
  	@include('head')
  </head>
  <body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      
      @include('header')

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      @include('sidebar')

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Meu Perfil
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-user"></i> Perfil</li>
          </ol>
        </section>        

        <!-- Main content -->
        <section class="content">

          @if (session('status'))
              <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ session('status') }}
              </div>
          @endif

          @if (session('error'))
              <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ session('error') }}
              </div>
          @endif
                    
          <form action="{{ url('update-profile') }}" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="col-md-6">
              <!-- Default box -->
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Meus dados</h3>
                </div>
                <div class="box-body">

                  <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
                      <label for="name">Nome</label>
                      <input type="text" class="form-control" placeholder="Nome" name="name" value="{{ \Auth::user()->name }}">
                      <span class="glyphicon glyphicon-user form-control-feedback"></span>
                      @if ($errors->has('name'))
                          <span class="help-block">
                              {{ $errors->first('name') }}
                          </span>
                      @endif
                  </div>

                  <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                      <label for="email">E-mail</label>
                      <input type="text" class="form-control" placeholder="E-mail" name="email" value="{{ \Auth::user()->email }}">
                      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                      @if ($errors->has('email'))
                          <span class="help-block">
                              {{ $errors->first('email') }}
                          </span>
                      @endif
                  </div>

                  <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                      <label for="username">Login</label>
                      <input type="text" class="form-control" placeholder="Login" name="username" value="{{ \Auth::user()->username }}">
                      <span class="glyphicon glyphicon-user form-control-feedback"></span>
                      @if ($errors->has('username'))
                          <span class="help-block">
                              {{ $errors->first('username') }}
                          </span>
                      @endif
                  </div>                

                  <a class="btn btn-default" href="{{ url('change-password') }}"><i class="fa fa-fw fa-key"></i> &nbsp; Trocar senha</a>

                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>

            <div class="col-md-6">
              <!-- Default box -->
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Foto do perfil</h3>
                </div>
                <div class="box-body">

                  <div class="form-group has-feedback {{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image">Imagem</label>
                    <input type="file" id="imagem" name="image">
                    <p class="help-block"></p>
                    @if ($errors->has('image'))
                        <span class="help-block">
                            {{ $errors->first('image') }}
                        </span>
                    @endif
                  </div>

                  <div class="user-image-panel">
                    <img id="image_preview" class="img-circle" src="{{ \Auth::user()->image }}">
                  </div>
                  
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>

            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
            
          </form>

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      @include('footer')

      <!-- Control Sidebar -->
      @include('control-sidebar')
    </div>
    <!-- ./wrapper -->

    @include('core-scripts')

    <script type="text/javascript">
      function readURL(input) {

          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#image_preview').attr('src', e.target.result);
              }

              reader.readAsDataURL(input.files[0]);
          }
      }

      $(document).ready(function() {
        $("#imagem").change(function(){
            readURL(this);
        });
      });
    </script>
  </body>
</html>
