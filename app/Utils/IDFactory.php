<?php

/*
| 
| @author Carlos Eduardo G. R. Ribas
|
*/

/*
|--------------------------------------------------------------------------
| IDFactory
|--------------------------------------------------------------------------
|
| Route for catch the /admin get request.
| The access to the dashboard panel is allowed for administrators only.
| The user type is defined by the 'role' attribute that could be 'USER' or 'ADMIN'
|
*/
class IDFactory {
	
	/*
	|--------------------------------------------------------------------------
	| create()
	|--------------------------------------------------------------------------
	|
	| Route for catch the /admin get request.
	| The access to the dashboard panel is allowed for administrators only.
	| The user type is defined by the 'role' attribute that could be 'USER' or 'ADMIN'
	|
	*/
	public static function create()
	{		
		return md5(uniqid(mt_rand(), true));
	}

}