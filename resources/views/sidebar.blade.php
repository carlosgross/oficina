<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MENU</li>
      <li class="{{ isset($dashboard) ? $dashboard : '' }}" ><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
      <li class="treeview {{ isset($usuario) ? $usuario : '' }}">
        <a href="#">
          <i class="fa fa-users"></i> <span>Clientes</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ isset($usuarios_cadastrados) ? $usuarios_cadastrados : '' }}"><a href="{{ url('user') }}"><i class="fa fa-database"></i> Clientes cadastrados</a></li>
          <li class="{{ isset($novo_usuario) ? $novo_usuario : '' }}"><a href="{{ url('user/create') }}"><i class="fa fa-user-plus"></i> Novo cliente</a></li>
        </ul>
      </li>      
      <li class="{{ isset($calendario) ? $calendario : '' }}">
        <a href="{{ url('calendario') }}">
          <i class="fa fa-calendar"></i> <span>Calendar</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-red">3</small>
            <small class="label pull-right bg-blue">17</small>
          </span>
        </a>
      </li>      
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>