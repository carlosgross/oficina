<!DOCTYPE html>
<html>
  <head>
  	@include('head')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('resources/assets/plugins/datatables/dataTables.bootstrap.css') }}">
  </head>
  <body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      
      @include('header')

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      @include('sidebar')

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Clientes cadastrados
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><i class="fa fa-users"></i> Clientes cadastrados</li>
          </ol>
        </section>        

        <!-- Main content -->
        <section class="content">

          @if (session('status'))
              <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ session('status') }}
              </div>
          @endif

          @if (session('error'))
              <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ session('error') }}
              </div>
          @endif

          <div class="header-buttom">
            <a class="btn btn-primary" href="{{ url('user/create') }}"><i class="fa fa-user-plus"></i> &nbsp; Novo cliente</a>
          </div>

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Clientes</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- <div class="table-responsive"> -->
                <table id="datable" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Data de cadastro</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)                        
                    <tr>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td data-order="{{ strtotime($user->created_at) }}">{{ date('d/m/Y H:i:s', strtotime($user->created_at)) }}</td>                    
                      <td>
                        <a href="{{ url('user/'.$user->id.'/edit') }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                        <a class="btn btn-danger btn-xs remove-resource" data-toggle="modal" data-target="#myModal" data-title="Clientes" data-text="Deseja remover o Cliente: {{ $user->name }} ?" data-name="{{ $user->name }}" data-url="{{ url('user/'.$user->id) }}"><i class="fa fa-trash-o"></i> Remover </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              <!-- </div> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->

        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body"></div>
              <div class="modal-footer">
                <form id="remove-form" method="POST" action="">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Remover</button>
                </form>
              </div>
            </div>
            
          </div>
        </div>

      </div>
      <!-- /.content-wrapper -->

      @include('footer')

      <!-- Control Sidebar -->
      @include('control-sidebar')
    </div>
    <!-- ./wrapper -->

    @include('core-scripts')

    <!-- DataTables -->
    <script src="{{ asset('resources/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('resources/assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
      $(document).ready(function() {

        $("#datable").DataTable({
          language: {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "Não há Clientes cadastrados.",
            "sInfo":         "Monstrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix":  "",
            "sSearch":       "Buscar: ",
            "sUrl":          "",
            "oPaginate": {
              "sFirst":    "Primeiro",
              "sPrevious": "Anterior",
              "sNext":     "Próximo",
              "sLast":     "Último"
            }
          },
          columnDefs: [
             { orderable: false, targets: -1 }
          ]
        });

        $('.remove-resource').on("click", function () {
          var name = $(this).attr('data-name');
          var url = $(this).attr('data-url');
          var title = $(this).attr('data-title');
          var text = $(this).attr('data-text');

          $('#myModal').find('.modal-title').html(title);
          $('#myModal').find('.modal-body').html('<p>'+text+'</p>');
          $('#remove-form').attr('action',url);
        });

      });
    </script>
  </body>
</html>
