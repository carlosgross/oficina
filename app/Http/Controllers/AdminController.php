<?php

/*
| 
| @author Carlos Eduardo G. R. Ribas
|
*/

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Validator;
use App\User;

require_once app_path().'/Utils/IDFactory.php';
require_once app_path().'/Utils/ImageUtils.php';

class AdminController extends Controller {

	public function dashboard()
	{
		if(\Auth::user()->role == "ADMIN")
			return view('/index', ['dashboard' => 'active']);
	}

	public function login()
	{
		return view('auth/login');
	}	

	public function profile()
	{
		return view('profile');
	}

	public function update_profile(Request $request)
	{

	    $validator = Validator::make($request->all(), [
	      'name' => 'required|max:255',
	      'email' => 'required|email|max:255',
	      'username' => 'required|max:255',
	      'image' => 'image',
	    ]);

	    if ($validator->fails()) {
	        return redirect('perfil')->withErrors($validator)->withInput();
	    }

	    $duplicate_email = User::where('email','=',$request->input('email'))->where('id','<>',\Auth::user()->id)->get();
	    $duplicate_username = User::where('username','=',$request->input('username'))->where('id','<>',\Auth::user()->id)->get();

	    if(count($duplicate_email) > 0) {
	      return redirect('perfil')->with('error', 'E-mail já utilizado por outro usuário');
	    }

	    if(count($duplicate_username) > 0) {
	      return redirect('perfil')->with('error', 'Login já utilizado por outro usuário');
	    }

	    $user = User::find(\Auth::user()->id);

	    $user->name = $request->input('name');
	    $user->email = $request->input('email');
	    $user->username = $request->input('username');

	    if($request->hasFile('image')) {
	      // remove old image folder
      	  $this->delTree('public/upload/users/'.$user->id);

	      $thumbnailImage = $request->file('image');
	      $filename = $thumbnailImage->getClientOriginalName();
	      $destinationPath = 'public/upload/users/'.$user->id;
	      $thumbnailImage->move($destinationPath, $filename);
	      $user->image = url('public/upload/users/'.$user->id.'/'.$filename);
	    }

	    $user->save();

	    return redirect('perfil')->with('status', 'Seus dados foram atualizados com sucesso!');
	}

	public function view_change_password() {
		return view('change-password');
	}

	public function change_password(Request $request) {
		$validator = Validator::make($request->all(), [
		  'current_password' => 'required',
		  'password' => 'required|min:6|confirmed',
		]);

		if ($validator->fails()) {
		    return redirect('change-password')->withErrors($validator)->withInput();
		}

		// Password does not match
		if (!\Hash::check($request->input('current_password'), \Auth::user()->password)) {
		    return redirect('change-password')->with('error', 'Senha atual está incorreta');
		}

		$user = User::find(\Auth::user()->id);

		$user->password = \Hash::make($request->input('password'));

		$user->save();		

		return redirect('change-password')->with('status', 'Sua senha foi alterada com sucesso!');
	}

	public function resetPassword() {
		return view('auth/passwords/email');
	}

	public function calendar()
	{
		return view('calendar', ['calendario' => 'active']);
	}

	// remove folder
	private function delTree($dir) { 
	    $files = array_diff(scandir($dir), array('.','..')); 
	    foreach ($files as $file) { 
	      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
	    } 
	    return rmdir($dir); 
	}
}
